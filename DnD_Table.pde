Controller[][] grid;
int spacer,gridSize;

void setup(){
  size(800,800);
  gridSize = 800;
  spacer = 100;
  grid = new Controller[gridSize/spacer][gridSize/spacer];
  for (int y = 0; y < gridSize/spacer; y+= 1) {
    for (int x = 0; x < gridSize/spacer; x+= 1) {
        grid[x][y] = new Controller("", color(0,95,0));
    }
  }
  grid[5][7] = new Controller("Zach", color(0,255,0));
  grid[6][7] = new Controller("Mike", color(255,0,0));
  grid[5][6] = new Controller("Darude", color(0,0,255));
  strokeWeight(1);
  noLoop();
}

void draw() {
  background(51);
  for (int y = 0; y < gridSize/spacer; y += 1) {
    for (int x = 0; x < gridSize/spacer; x += 1) {
      Controller c = grid[x][y];
      stroke(c.trimColor);
      if(c.name.equals("")){
        fill(c.trimColor, 50);
        rect((x*spacer)+1,(y*spacer)+1,spacer,spacer);
      }else{
        fill(c.trimColor);
        ellipse((x*spacer)+(spacer/2)+1,(y*spacer)+(spacer/2)+1,spacer-2,spacer-2);
      }
    }
  }
}
/*TODO:
*  Class: Controller, PlayerController, and NonPlayerController
*  DMController with Commandbar
*
*/